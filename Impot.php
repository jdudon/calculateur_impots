<?php 
class Impot {
    const TAUX_BAS = 0.15;
    const TAUX_HAUT = 0.20;

    private $nom;
    private $revenu;

    public function __construct($nom, $revenu) {
        $this->nom = $nom;
        $this->revenu = $revenu;
    }

    public function calculImpot() {
        if ($this->revenu < 15000) {
            return $this->revenu * self::TAUX_BAS;
        } else {
            return $this->revenu * self::TAUX_HAUT;
        }
    }

    public function afficheImpot() {
        $montantImpot = $this->calculImpot();
        return "Mr/Mme " . $this->nom . " votre impôt est de " . $montantImpot . " euros.";
    }
}